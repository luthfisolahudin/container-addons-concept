const AVAILABLE_CONTAINER_OPTIONS = {
    colors: [
        'blue',
        'turquoise',
        'green',
        'yellow',
        'orange',
        'red',
        'pink',
        'purple',
    ],
    icons: [
        'fingerprint',
        'briefcase',
        'dollar',
        'cart',
        'circle',
        'gift',
        'vacation',
        'food',
        'fruit',
        'pet',
        'tree',
        'chill',
        'fence',
    ],
}

const getAvailableOptions = (name) => {
    return AVAILABLE_CONTAINER_OPTIONS[`${name}s`] || undefined
}

const containerQuery = async (name = undefined) => {
    const query = {}

    if (typeof name !== 'undefined') {
        query['name'] = name
    }

    return await browser.contextualIdentities.query(query)
}

const containerTotal = async () => {
    const containers = await containerQuery()

    return containers.length
}

const PLACEHOLDERS = {
    CURRENT: async (totalAvailableValue) => {
        const total = await containerTotal()

        return totalAvailableValue
            ? total % totalAvailableValue
            : total
    },
    NEXT_INCREMENT: async (totalAvailableValue) => {
        const total = await containerTotal()
        const nextIncrement = total + 1

        return totalAvailableValue
            ? nextIncrement % totalAvailableValue
            : nextIncrement
    },
}

const placeholderNeedle = (placeholder) => `{${placeholder}}`

const resolvePlaceholder = async (value, of) => {
    for (const placeholder in PLACEHOLDERS) {
        const needle = placeholderNeedle(placeholder)

        if (!value.includes(needle)) {
            continue
        }

        const getValue = PLACEHOLDERS[placeholder]
        const totalAvailableValue = getAvailableOptions(of)?.length

        value = value.replace(needle, await getValue(totalAvailableValue))
    }

    return value
}

const CONTAINER_DEFAULT_OPTIONS = {
    name: '({NEXT_INCREMENT})',
    color: '{CURRENT}',
    icon: 'briefcase',
}

const containerOptions = async () => {
    const options = structuredClone(CONTAINER_DEFAULT_OPTIONS)

    for (const optionName in options) {
        const placeholder = placeholderNeedle(optionName)
        const hasPlaceholder = Object.keys(PLACEHOLDERS)
            .find((placeholder) => options[optionName].includes(placeholder))

        if (!hasPlaceholder) {
            continue
        }

        let placeholderValue = await resolvePlaceholder(options[optionName], optionName)
        const pluralName = `${optionName}s`
        const isInAvailableOptions = Object.keys(AVAILABLE_CONTAINER_OPTIONS).includes(pluralName)

        if (isInAvailableOptions) {
            placeholderValue = AVAILABLE_CONTAINER_OPTIONS[pluralName][placeholderValue]
        }

        options[optionName] = placeholderValue
    }

    return options
}

const createContainer = async () => {
    return await browser.contextualIdentities.create(await containerOptions())
}

const createTabInContainer = async (containerId) => {
    return await browser.tabs.create({
        active: true,
        cookieStoreId: containerId,
    })
}

browser.commands.onCommand.addListener(async (command) => {
    if (command === 'open-new-tab-in-new-container') {
        const container = await createContainer()
        const containerId = container.cookieStoreId

        createTabInContainer(containerId)
    }
})
